# Linear Regression - Integrated Homomorphic Encryption and Intel SGX with Graphene

Please note that due to changes in Graphine/Gramine, instructions and manifest syntax for compilation and running with Graphene/Gramine may be outdated. This work was originally undertaken in Summer 2021. Contact jtakeshi@nd.edu for support. A preprint of our manuscript using this work is [here](https://eprint.iacr.org/2021/1155), and is also included in this repository.

Dependencies:

- PALISADE
    - [Github Repository](https://gitlab.com/palisade/palisade-development)
- Graphene
    - [Build Directions](https://graphene.readthedocs.io/en/latest/)
- SGX SDK and PSW
    - [Github Repository](https://github.com/intel/linux-sgx) 

We recommend this be run on [Ubuntu 18.04](https://releases.ubuntu.com/18.04) for best compatability with all tools, though it may be possible to use other distributions. A SGX-equipped processor is required (directions on how to find if yours has SGX are [here](https://www.intel.com/content/www/us/en/support/articles/000028173/processors.html)) to use the SGX portion, though Graphene/Gramine can be used without SGX.

# Installation of software
See SGX\_Graphine\_guide.md for instructions on how to set up your machine to use SGX, install the SGX SDK and PSW, Graphine/Gramine, and PALISADE. These instructions were originally written for Graphene; note that the newer version named Gramine may have some differences. When building PALISADE Development, use `-DBUILD_STATIC=ON` to build static libraries. This prevents later having to specify the shared libraries as accessible in the manifest file.

# Build GPS ordinary least squares example
1. `make`
2. `cd bin`
3. `make SGX=1 -f mk_graphene inverse.manifest.sgx inverse.token pal_loader`

In general, the programs use the `-n` flag to specify the number of data points and `-p` to specify the number of dependent variables; if each program is run independently then these must be identical across invocations, and the programs should be run in the correct order specified below

# Run a timing test for set parameters

## 1. Generate random data with specified n (users) and p (feature values) <br/>
-   `./makeData -n [value] -p [value]`

    - This creates `ctexts/dependent.ctext, ctexts/original.ctext, ctexts/tranpose.ctext`
    - Also creates PALISADEContainer in container directory



## 2. Run X (transpose) times X and write result to file <br />
- `./firstMult -n [value] -p [value]`

    - This creates `product_left.ctext` (will be read into SGX)
    - Note: values of p and n must be <= the values specified in makeData


## 3. Run X (transpose) times y and write result to file <br />
-   `./secondMult -n [value] -p [value] `

    - This creates product_right.ctext (will be read into SGX)
    - Note: values of p and n must be <= the values specified in makeData


## 4. Compute inverse of left product (X'X) and multiply by right product (X'y) to get beta vector in SGX <br />
- `SGX=1 OMP_NUM_THREADS=1 ./pal_loader ./inverse -p [value] -n [value]` <br />


  - Final result of linear regression (beta vector) ouputted to result/beta.txt

# To run an overall battery of tests of linear regression and capture timing: <br />
- `./timing.py` <br />
    -  This will run a test of the linear regression program with varying p and n values:
        -  p ranges from 2-12
        -  n ranges from 1,000,000 to 10,000,000 in 1,000,000 increments
    -  Timing results will be written to:
        -  `result/firstMult.csv`
        -  `result/secondMult.csv`
        -  `result/sgx.csv`
