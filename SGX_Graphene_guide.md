# SGX and Graphene for PALISADE
# Jonathan S. Takeshita, University of Notre Dame [jtakeshi@nd.edu](jtakeshi@nd.edu)
This document describes how to run an application with PALISADE on SGX using Graphene. Basic knowledge of all 3 platforms is assumed.
When running any make commands, the -j argument can be used to specify the number of threads to use. This is strongly suggested to speed up compilation, but should be turned off to identify the source of a build failure.
This is not a guide for making a production-ready system -- research purposes only! Please note that Graphine (now called Gramine) has undergone significant changes since this document was written, and the current installation and manifest usage may differ.
Provided by the Data, Security, and Privacy Lab at The University of Notre Dame.
Included files are:

- mk\_graphene, a Graphene/SGX Makefile for an application called "run\_computation"
- run_computation.manifest.template, a Graphene manifest template for the same application

# System Setup
This guide is for Ubuntu 18.04 LTS with an updated kernel (5.11 or later). Other systems probably can be made to work, but haven't been tested.

1. Install Ubuntu 18.04 LTS. Download the image from [here](https://releases.ubuntu.com/18.04/), then use dd or another utility to copy it to a USB flash drive. Insert the flash drive to your target system, boot, and press the BIOS menu key (variously F11, F12, F2, or other depending on system). Use the USB drive to boot, then follow the GUI instructions for installing Ubuntu. DO NOT UPGRADE TO 20.04!
2. Upgrade the kernel: follow [this guide](https://ubuntuhandbook.org/index.php/2021/02/linux-kernel-5-11released-install-ubuntu-linux-mint/), using the "generic" headers and modules. After rebooting, use `uname -r` to verify that the kernel version has been upgraded. If needed, you can still select the original kernel from GRUB when booting.
3. Enable SGX in the BIOS, if not already done. This will require a reboot.

# Install SGX SDK and PSW
Follow the [installation guide](https://github.com/intel/linux-sgx) . Their organization is confusing, so here are the steps needed:

1. Prerequisites: run ` sudo apt install build-essential ocaml ocamlbuild automake autoconf libtool wget python libssl-dev git cmake perl libcurl4-openssl-dev protobuf-compiler libprotobuf-dev debhelper reprepro unzip`
2. Clone the repo, cd to it, and run `make preparation`. This will download mitigation tools, which should be installed with ` sudo cp external/toolset/ubuntu18.04/{as,ld,ld.gold,objdump} /usr/local/bin/`
3. Build the SDK with `make sdk`, then `make sdk_install_pkg`. Then, run the installer at `linux/installer/bin/sgx_linux_x64_sdk_${version}.bin`
4. Similarly, build the PSW with `make psw`, `make deb_psw_pkg`, and run the installer at `linux/installer/bin/sgx_linux_x64_psw_${version}.bin`
5. If not running, start the aesmd service: `sudo service aesmd start`. A reboot is also probably a good idea.

# Install Graphene-SGX
The following is the original instructions from Graphene, which may now be made obsolete due to Gramine's apt-based [installation](https://gramine.readthedocs.io/en/latest/quickstart.html). This new method is probably better; the original instructions are below for reference.
Follow the [instructions](https://graphene.readthedocs.io/en/latest/quickstart.html#quick-start-with-sgx-support). Make sure to use their suggested methods to verify that SGX drivers and SDK are properly installed. 
Use `ISGX_DRIVER_PATH=""`, because kernel 5.11 has the in-driver kernel. Skip setting `vm_mmap_min_addr=0`, as we didn't install DCAP. For the meson build, use `-Dsgx=enabled`, and also `-Ddirect=disabled` unless you also want to build basic Graphene.

- Don't forget to copy the enclave key to `Pal/src/host/Linux-SGX/signer/enclave-key.pem`
- Note that this install may have to be redone for different users, as the pal_loader file created for applications will symlink to the original repository and not any system file.
- The GCC and curl tests are the easiest to run. Use `make SGX=1 check` in their directories.

# Install PALISADE development version
Follow the [guide](https://gitlab.com/palisade/palisade-development/-/wikis/Instructions-for-building-PALISADE-in-Linux). We will build the regular version using OpenMP, and can disable threads through runtime later.

# Create the template manifest for your application
It is recommended that your application work out of a single folder. Copy the manifest file `[app].manifest.template`, and the Makefile, and modify both to work with your application. 

- (JST: this may or may not be necessary anymore.) Compile your application as a single-threaded program, i.e. omit `-pthread -fopenmp`. Also, `-no-pie` may also be needed.
- Replace the application name where appropriate in the manifest files and Makefiles.
- Make sure that /usr/local/lib or the directory containing the PALISADE .so libraries used to compile your application is argued in `[app].manifest.template` in the `LD_LIBRARY_PATH`, and that the specific libraries used are set as trusted files. See the sample manifest template for an example.
- Specify any other files and directories for data, inputs, outputs, keys/parameters, etc.
- Set `loader.insecure__use_cmdline_argv = 1` (if you want your app to read CLI arguments).
- Set `loader.insecure__use_host_env = 1` to disable multithreading via environment variables.
# Last steps
Create the signatures, token, etc. with `make SGX=1 -f mk_graphene run_computation.manifest.sgx run_computation.token pal_loader`
Then, run `SGX=1 OMP_NUM_THREADS=1 ./pal_loader ./app [args]`.
